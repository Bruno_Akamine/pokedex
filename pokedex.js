function mudatudo(){
    const body=document.querySelector('body');
    const type=document.querySelector('body').classList;
    const bot=document.querySelector('#botao');
    if(type=='light'){
        body.classList.remove('light');
        body.classList.add('dark');
        bot.classList.remove('lightbot');
        bot.classList.add('darkbot');
        document.querySelector('#botao').innerHTML='Light Mode';
    }
    else{
        body.classList.remove('dark');
        body.classList.add('light');
        bot.classList.remove('darkbot');
        bot.classList.add('lightbot');
        document.querySelector('#botao').innerHTML='Dark Mode';
    }
}
function press(){
    const pokemon=document.querySelector('.search').value;
    search(pokemon);
}
function enter(e){
    const pokemon=document.querySelector('.search').value;
    if(e.keyCode==13){
        search(pokemon)
    }
    return
}

async function search(pokemon){


    const lower=pokemon.toLowerCase()
    const url='https://pokeapi.co/api/v2/pokemon/' + lower
    const resp= await fetch(url)

    if(!resp.ok){
        if(resp.status==404){
            alert('Pokemon não encontrado')
        }
        else{
            alert('Erro na requisição')
        }
        
        return
    }
    const json= await resp.json()
    console.log(json)
    const habilidades=json.abilities;
    const status=json.stats;
    console.log(status)
    const nome=fisrtlettertoupper(json.name)
    const type=fisrtlettertoupper(json.types[0].type.name)
    document.querySelector('.nome').innerHTML=nome
    document.querySelector('#image').src=json.sprites.front_default;
    document.querySelector('.tipo').innerHTML=type;
    const abi=concathab(habilidades)
    document.querySelector('.habilidade').innerHTML=abi;
    const stat=concatstat(status)
    document.querySelector('.status').innerHTML=stat;
}
function fisrtlettertoupper(word){
    let fisrtletter=word.charAt(0);
    const uppercase=fisrtletter.toUpperCase();
    const withoutfirst=word.slice(1);
    const final=uppercase+withoutfirst;
    return final;
}
function concathab(vet){
    let final='';
    for(let i=0;i<vet.length;i++){
        if(i==vet.length-1){
            final+=fisrtlettertoupper(vet[i].ability.name)
        }
        else{
            final+=fisrtlettertoupper(vet[i].ability.name)+', '
        }
    }
    return final
}
function concatstat(vet){
    let final='';
    for(let i=0;i<vet.length;i++){
        if(i==vet.length-1){
            final+=fisrtlettertoupper(vet[i].stat.name)+'='+vet[i].base_stat;
        }
        else{
            final+=fisrtlettertoupper(vet[i].stat.name)+'='+vet[i].base_stat+'; ';
        }
    }
    return final;
}